# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

do_install:append() {
        # Oniro only have /home/oniro which is not for "weston" user
	sed "s|\( *WorkingDirectory=.*\)|# \1 # Oniro does not use this|g" \
	    -i "${D}${systemd_system_unitdir}/weston.service"
}
