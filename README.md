<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# meta-oniro-blueprints-vending-machine
Welcome to the Oniro Blueprint for a Vending Machine.

Oniro is an Eclipse Foundation project focused on the development of a
distributed open source operating system for consumer devices.

*\*Oniro is a trademark of Eclipse Foundation.*

In this repository you can find the necessary yocto layers to run the 
Vending Machine blueprint.

Documentation for this blueprint can be found on the [Oniro Project Blueprints 
documentation page](https://docs.oniroproject.org/projects/blueprints/vending-machine.html).

## Set up your workspace
The following instructions shows how to set up the Oniro Project workspace, 
please visit the [Oniro docs](
https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html
) for more details.

```console
$ mkdir ~/oniroproject; cd ~/oniroproject
$ repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b kirkstone
$ repo sync --no-clone-bundle
$ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux
```

## Add the Vending Machine blueprint layers to your build
Clone this repository into your workspace

```console 
$ git clone --recurse-submodules https://gitlab.eclipse.org/eclipse/oniro-blueprints/vending-machine/meta-oniro-blueprints-vending-machine.git ~/oniroproject/meta-oniro-blueprints-vending-machine
```

Add the layers to your yocto build
```console
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-vending-machine/meta-oniro-blueprints-core
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-vending-machine/meta-oniro-blueprints-vending-machine
```

## Build the blueprint image

Build the Oniro image with the Vending Machine blueprint 

```console
$ DISTRO=oniro-linux-blueprint-vending-machine MACHINE=raspberrypi4-64 bitbake blueprint-vending-machine-image
```

# Contributing

## Merge requests

All contributions are to be handled as merge requests in the
[meta-oniro-blueprints-vending-machine Gitlab repository](
https://gitlab.eclipse.org/eclipse/oniro-blueprints/vending-machine/meta-oniro-blueprints-vending-machine
). 
For more information on the contributing process, check the `CONTRIBUTING.md` file.

## Maintainers

* Andrei Gherzan <andrei.gherzan@huawei.com>
* Stefan Schmidt <stefan.schmidt@huawei.com>
* Francesco Pham <francesco.pham@huawei.com>

# License

This layer is release under the licenses listed in the `LICENSES` root directory.
